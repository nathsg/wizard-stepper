import { Component, QueryList, AfterViewChecked, ViewChildren, ChangeDetectorRef } from '@angular/core';
import { StepComponent } from './wizard/step/step.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewChecked {

  @ViewChildren(StepComponent)
  steps: QueryList<StepComponent>;

  stepsNumber: number;
  title = 'wizard';
  completed: number;

  constructor(private cdRef: ChangeDetectorRef) { }

  ngAfterViewChecked() {
    const stepsArray = this.steps.toArray()

    this.stepsNumber = stepsArray.length
    this.completed = stepsArray.findIndex(step => step.active === true)
    console.log(this.stepsNumber, this.completed)
    this.cdRef.detectChanges();
  }

  previous() {
    this.stepsNumber = 1
  }
}
