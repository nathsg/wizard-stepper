import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WizardComponent } from './wizard/wizard.component';
import { StepComponent } from './wizard/step/step.component';
import { StepperComponent } from './stepper/stepper.component';
@NgModule({
  declarations: [
    AppComponent,
    WizardComponent,
    StepComponent,
    StepperComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
