import { ContentChildren, Component, HostBinding, QueryList, AfterViewInit } from '@angular/core';
import { StepComponent } from './step/step.component';

@Component({
  selector: 'app-wizard',
  templateUrl: './wizard.component.html',
  styleUrls: ['./wizard.component.scss']
})
export class WizardComponent implements AfterViewInit {
  @ContentChildren(StepComponent)
  steps: QueryList<StepComponent>;

  private activeIndex;

  ngAfterViewInit() {
    this.goTo(0);
  }

  previous() {
    this.goTo(this.activeIndex - 1);
  }

  next() {
    this.goTo(this.activeIndex + 1);
  }

  goTo(index) {
    const steps = this.steps.toArray();

    if (index >= 0 && index < steps.length) {
      steps.forEach((step, i) => {
        if (i < index) {
          step.completed = true;
        } else {
          step.completed = false;
        }
        step.active = false;
      });

      steps[index].active = true;
      this.activeIndex = index;
    }
  }

  @HostBinding('class.ng-wizard')
  get ngWizardClass() {
    return true;
  }
}
