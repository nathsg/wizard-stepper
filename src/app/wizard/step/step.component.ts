import { ChangeDetectorRef, Component, HostBinding, OnInit } from '@angular/core';

@Component({
  selector: 'app-step',
  templateUrl: './step.component.html',
  styleUrls: ['./step.component.scss']
})
export class StepComponent implements OnInit {
  completed: boolean = false;
  active: boolean = false;

  constructor(private cdRef: ChangeDetectorRef) { }

  ngOnInit() {
    this.cdRef.detectChanges();
  }

  @HostBinding('class.ng-wizard-step')
  get ngWizardClass() {
    return true;
  }

  @HostBinding('class.ng-wizard-step--active')
  get ngWizardActiveClass() {
    return this.active;
  }

  @HostBinding('class.ng-wizard-step--completed')
  get ngWizardCompletedClass() {
    return this.completed;
  }
}
