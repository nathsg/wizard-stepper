import { AfterViewInit, Component, Input } from '@angular/core';

@Component({
  selector: 'app-stepper',
  templateUrl: './stepper.component.html',
  styleUrls: ['./stepper.component.scss']
})
export class StepperComponent implements AfterViewInit {
  @Input()
  completed: number = 0;

  @Input()
  stepsNumber: number = 0;

  width: string;
  range: Array<number>;

  ngAfterViewInit() {
    this.range = [...Array(this.stepsNumber).keys()];
    this.setWidth();
  }

  private setWidth() {
    this.width = `calc(100%/${this.stepsNumber} - 10px)`;
  }
}
